package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UsersWithScheduledMeetingsListDTO {
    @JsonProperty("user")
    public UserDTO user;
    @JsonProperty("meetings")
    public UserMeetingDTO[] meetings;
}
