package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class MeetingCreateDTO {
    @JsonProperty("topic")
    public String topic;

    @JsonProperty("type")
    public Integer type;

    @JsonProperty("start_time")
    public String startTime;

    @JsonProperty("duration")
    public Integer duration;

    @JsonProperty("timezone")
    public String timezone;

    @JsonProperty("password")
    public String password;

    @JsonProperty("agenda")
    public String agenda;

    @JsonProperty("registrants_email_notification")
    public Boolean registrantsEmailNotification;

    @JsonProperty("recurrence")
    public Object recurrence;

    @JsonProperty("settings")
    public Object settings;
}
