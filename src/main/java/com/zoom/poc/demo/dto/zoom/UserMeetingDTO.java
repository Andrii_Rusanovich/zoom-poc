package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserMeetingDTO {
    @JsonProperty("uuid")
    public String uuid;

    @JsonProperty("id")
    public Long id;

    @JsonProperty("host_id")
    public String hostId;

    @JsonProperty("topic")
    public String topic;

    @JsonProperty("type")
    public Integer type;

    @JsonProperty("start_time")
    public String startTime;

    @JsonProperty("duration")
    public Integer duration;

    @JsonProperty("timezone")
    public String timezone;

    @JsonProperty("created_at")
    public String createdAt;

    @JsonProperty("join_url")
    public String joinUrl;

    @JsonProperty("agenda")
    public String agenda;
}
