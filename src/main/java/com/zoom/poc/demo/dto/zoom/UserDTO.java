package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonIgnoreProperties
public class UserDTO {

    @JsonProperty("id")
    public String id;

    @JsonProperty("first_name")
    public String firstName;

    @JsonProperty("last_name")
    public String lastName;

    @JsonProperty("email")
    public String email;

    //enum for future
    @JsonProperty("type")
    public Integer type;

    @JsonProperty("pmi")
    public Long pmi;

    @JsonProperty("timezone")
    public String timezone;

    @JsonProperty("verified")
    public Integer verified;

    @JsonProperty("dept")
    public String dept;

    @JsonProperty("created_at")
    public String createdAt;

    //"2018-11-15T01:10:08Z"
    @JsonProperty("last_login_time")
    public String lastLoginTime;

//    //"4.4.55383.0716(android)"
//    @JsonProperty("last_client_version")
//    public String lastClientVersion;
//
//    @JsonProperty("pic_url")
//    public String picUrl;
//
//    @JsonProperty("im_group_ids")
//    public String[] imGroupIds;

    @JsonProperty("language")
    public String language;

    @JsonProperty("phone_number")
    public String phoneNumber;



    @JsonProperty("status")
    public String status;
}

//{"id":"EeTZqCCLQQCnsAdy2DCWQg",
//        "first_name":"OpenExchange",
//        "last_name":"Development",
//        "email":"zoom-development@dev-openexc.com",
//        "type":2,
//        "pmi":2429632373,
//        "timezone":"America/New_York",
//        "verified":1,
//        "created_at":"2020-04-07T20:25:04Z",
//        "last_login_time":"2020-04-07T20:25:04Z",
//        "language":"",
//        "phone_number":"",
//        "status":"active"}