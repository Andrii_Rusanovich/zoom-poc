package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
public class UserListDTO {

    @JsonProperty("page_count")
    public Integer pageCount;

    @JsonProperty("page_number")
    public Integer pageNumber;

    @JsonProperty("page_size")
    public Integer pageSize;

    @JsonProperty("total_records")
    public Integer totalRecords;

    @JsonProperty("users")
    public List<UserDTO> users;
}
