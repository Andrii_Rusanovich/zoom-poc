package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserMeetingsListDTO {
    @JsonProperty("total_records")
    public Integer totalRecords;

    @JsonProperty("page_number")
    public Integer pageNumber;

    @JsonProperty("page_size")
    public Integer pageSize;

    @JsonProperty("page_count")
    public Integer page_count;

    @JsonProperty("meetings")
    public UserMeetingDTO[] meetings;
}
