package com.zoom.poc.demo.dto.zoom;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class MeetingDTO {

    @JsonProperty("id")
    public Integer id;

    @JsonProperty("topic")
    public String topic;

    @JsonProperty("type")
    public Integer type;

    @JsonProperty("start_time")
    public String startTime;

    @JsonProperty("duration")
    public Integer duration;

    @JsonProperty("timezone")
    public String timezone;

    @JsonProperty("password")
    public String password;

    @JsonProperty("agenda")
    public String agenda;

    @JsonProperty("start_url")
    public String startUrl;

    @JsonProperty("join_url")
    public String joinUrl;

    @JsonProperty("h323_password")
    public String h323_password;

    @JsonProperty("pmi")
    public Integer pmi;

    @JsonProperty("tracking_fields")
    public Object[] tracking_fields;

    @JsonProperty("occurrences")
    public Object[] occurrences;

    @JsonProperty("settings")
    public Object settings;

    @JsonProperty("recurrence")
    public Object recurrence;
}
