package com.zoom.poc.demo.controller;

import com.zoom.poc.demo.dto.zoom.UserMeetingsListDTO;
import com.zoom.poc.demo.dto.zoom.UserListDTO;
import com.zoom.poc.demo.dto.zoom.MeetingCreateDTO;
import com.zoom.poc.demo.service.ZoomApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class ZoomController {

    @Autowired
    private ZoomApiService zoomApiService;

    @GetMapping("test")
    public String test() {
        return "42";
    }

    @GetMapping("users")
    public ResponseEntity<UserListDTO> getUsers() {
        UserListDTO userListDTO = zoomApiService.getAllUsers();
        return ResponseEntity.ok(userListDTO);
    }

    @GetMapping("/users/{userId}/meetings")
    public ResponseEntity<UserMeetingsListDTO> getScheduledMeetingsForUser(@PathVariable String userId) {
        //String userId = "SiUTYHkDSyapochWhLCUlw";
        UserMeetingsListDTO userMeetingsListDTO = zoomApiService.getAllScheduledMeetingsForUser(userId);
        return ResponseEntity.ok(userMeetingsListDTO);
    }

    @PostMapping("/users/{userId}/meetings/create")
    public String scheduleMettingForUser(@PathVariable String userId) {
        MeetingCreateDTO meetingCreateDTO = new MeetingCreateDTO();
        meetingCreateDTO.topic = "My conferention";
        meetingCreateDTO.type = 2; // Scheduled meeting default
        meetingCreateDTO.startTime = "2020-04-11GMT+312:00:00";
        meetingCreateDTO.duration = 60;
        meetingCreateDTO.timezone = "UTC+3";
        meetingCreateDTO.password = "12345abc";
        meetingCreateDTO.agenda = "My scheduled meeting";

        return zoomApiService.scheduleMeeting(userId, meetingCreateDTO);
    }

    @PostMapping("/webhook/meeting/update")
    public String consumeMeetingUpdateEvent(@RequestBody String json) {
        System.out.println(json);
        return "O3xh0kiKQX-yzptdp_Iv8Q";
    }
}
