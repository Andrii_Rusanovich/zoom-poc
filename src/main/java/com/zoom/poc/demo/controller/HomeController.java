package com.zoom.poc.demo.controller;

import com.zoom.poc.demo.dto.zoom.UserDTO;
import com.zoom.poc.demo.dto.zoom.UserListDTO;
import com.zoom.poc.demo.dto.zoom.UsersWithScheduledMeetingsListDTO;
import com.zoom.poc.demo.service.ZoomApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private ZoomApiService zoomApiService;

    @GetMapping("/")
    public String index(Model model) {
        UserListDTO userListDTO = zoomApiService.getAllUsers();
        List<UsersWithScheduledMeetingsListDTO> listUsersWithScheduledMeetings = new ArrayList<>();
        for(UserDTO userDTO : userListDTO.users) {
            UsersWithScheduledMeetingsListDTO usersWithScheduledMeetingsListDTO = new UsersWithScheduledMeetingsListDTO();
            usersWithScheduledMeetingsListDTO.user = userDTO;
            usersWithScheduledMeetingsListDTO.meetings = zoomApiService.getAllScheduledMeetingsForUser(userDTO.id).meetings;
            listUsersWithScheduledMeetings.add(usersWithScheduledMeetingsListDTO);
        }
        model.addAttribute("listUsersWithScheduledMeetings", listUsersWithScheduledMeetings);

        return "index";
    }

}
