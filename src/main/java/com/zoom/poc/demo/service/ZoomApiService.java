package com.zoom.poc.demo.service;

import com.zoom.poc.demo.dto.zoom.UserMeetingsListDTO;
import com.zoom.poc.demo.dto.zoom.UserListDTO;
import com.zoom.poc.demo.dto.zoom.MeetingCreateDTO;

public interface ZoomApiService {
    UserListDTO getAllUsers();
    UserMeetingsListDTO getAllScheduledMeetingsForUser(String userId);
    String scheduleMeeting(String userId, MeetingCreateDTO meetingCreateDTO);
}
