package com.zoom.poc.demo.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoom.poc.demo.dto.zoom.MeetingCreateDTO;
import com.zoom.poc.demo.dto.zoom.UserListDTO;
import com.zoom.poc.demo.dto.zoom.UserMeetingsListDTO;
import com.zoom.poc.demo.service.ZoomApiService;
import io.fusionauth.jwt.Signer;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.hmac.HMACSigner;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Service
public class ZoomApiServiceImpl implements ZoomApiService {

    @Value("${zoom.jwt-app.public-key}")
    private String apiKey;

    @Value("${zoom.jwt-app.secret-key}")
    private String secretKey;

    private static String JWT_TOKEN = "";
    private String rootUrl = "https://api.zoom.us/v2";

    private ObjectMapper objectMapper;

    public ZoomApiServiceImpl() {
        this.objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    }

    @PostConstruct
    private void init() {
        JWT_TOKEN = getAccessToken();
    }

    @Override
    public UserListDTO getAllUsers() {
        //HttpClient httpClient = HttpClient.New(new "https://api.zoom.us/v2");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet loadUsersRequest = new HttpGet(this.rootUrl + "/users");
        //loadUsersRequest.addHeader("User-Agent", "Zoom-Jwt-Request");
        //loadUsersRequest.addHeader("content-type", "application/json");
        loadUsersRequest.addHeader("authorization", "Bearer " + JWT_TOKEN);
        try {
            HttpResponse response = httpClient.execute(loadUsersRequest);
            String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            return objectMapper.readValue(json, UserListDTO.class);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public UserMeetingsListDTO getAllScheduledMeetingsForUser(String userId) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String query = "/users/" + userId + "/meetings";
        HttpGet loadMeetingsRequest = new HttpGet(this.rootUrl + query);
        loadMeetingsRequest.addHeader("authorization", "Bearer " + JWT_TOKEN);
        try {
            HttpResponse response = httpClient.execute(loadMeetingsRequest);
            String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            return objectMapper.readValue(json, UserMeetingsListDTO.class);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public String scheduleMeeting(String userId, MeetingCreateDTO meetingCreateDTO) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String query = "/users/" + userId + "/meetings";
        HttpPost scheduleMeetingRequest = new HttpPost(this.rootUrl + query);
        scheduleMeetingRequest.addHeader("authorization", "Bearer " + JWT_TOKEN);

        try {
            String jsonBody = objectMapper.writeValueAsString(meetingCreateDTO);
            StringEntity requestEntity = new StringEntity(
                    jsonBody,
                    ContentType.APPLICATION_JSON);
            scheduleMeetingRequest.setEntity(requestEntity);

            HttpResponse response = httpClient.execute(scheduleMeetingRequest);
            return Integer.toString(response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    private String getAccessToken() {
        if (StringUtils.isEmpty(JWT_TOKEN)) {
            Signer signer = HMACSigner.newSHA256Signer(secretKey);

            // Build a new JWT with an issuer(iss), issued at(iat), subject(sub) and expiration(exp)
            JWT jwt = new JWT().setIssuer(apiKey)
                    .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
                    .setExpiration(ZonedDateTime.now(ZoneOffset.UTC).plusHours(8));

            // Sign and encode the JWT to a JSON string representation
            JWT_TOKEN = JWT.getEncoder().encode(jwt, signer);
        }
        return JWT_TOKEN;
    }
}
